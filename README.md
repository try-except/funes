# Funes v1.0

Funes is a python script that backs-up emails to Google Drive, using the Gmail
and GDrive APIs. Funes is in early development and subject to many changes.

This program is intended to run as a daemon and needs Domain-Wide delegation,
for information about how to give a Service Domain-Wide delegation, please refer
to the [Google API documentation](https://developers.google.com/identity/protocols/oauth2/service-account).

## To-Do List
- [ ] Thread backwards backup
- [ ] Scope reduction
- [ ] Project Number verification
- [ ] Single Shared Drive option
- [ ] Dryrun implementation
- [ ] Multiple Project Number support
- [ ] Label as Project Number marker
