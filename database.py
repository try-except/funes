from google.oauth2 import service_account #service account capability
from googleapiclient.discovery import build #build class
from parameters import *
import os.path
import sqlite3
from datetime import datetime

'''
    <--Funes v1.0-->
    Funes is a python daemon meant to help a Google Workspace admin backup the
    company emails.

    Copyright (C) 2021 Simón Jaramillo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''

def log_print(*args):
    '''
    Adds datetime.datetime.today() to every print for better logging
    '''
    print(datetime.today(), *args)

def create_database(database=DATABASE):
    conn = sqlite3.connect(database)
    cur = conn.cursor()
    cur.execute('''
        CREATE TABLE IF NOT EXISTS Users
        (user_email TEXT, historyId TEXT)''')
    cur.execute('''
        CREATE TABLE IF NOT EXISTS Projects
        (project_number INTEGER, driveId TEXT, mail_folderId TEXT)''')
    cur.execute('''
        CREATE TABLE IF NOT EXISTS Threads
        (threadId TEXT, project_number INTEGER)''')
    cur.close()
    conn.close()

def get_users(credentials, admin_account=ADMIN_ACCOUNT):
    '''Retrieves the users for the domain specified in parameters.py
    TODO: allow for multiple domains to be checked
    NEEDS: admin_account must be an admin account for the specified domains
    '''
    delegated_credentials = credentials.with_subject(admin_account)
    service = build('admin', 'directory_v1', credentials=delegated_credentials)
    results = service.users().list(domain=DOMAIN,orderBy='email').execute()
    users = results.get('users', [])

    if not users:
        log_print('No users in the domain.')
    else:
        log_print('Fetched users')
        for user in users:
            email = user['primaryEmail']
            fetched_user = Users.query_database(email)
            if not fetched_user:
                Users.add_new_user(email)
                log_print(f'added user {email} to database')
            else:
                log_print(f'user {email} already in database!')

class Users():

    def query_database(
        user_email,
        database=DATABASE):

        conn = sqlite3.connect(database)
        cur = conn.cursor()
        cur.execute('SELECT historyId FROM Users WHERE user_email = ?',
            (user_email, ))
        row = cur.fetchone()
        cur.close()
        conn.close()
        return row

    def update_historyId(
        user_email,
        new_historyId,
        database=DATABASE):

        conn = sqlite3.connect(database)
        with conn:
            #first check if the user is registered
            fetched_user = Users.query_database(user_email, database)
            if fetched_user:
                log_print('Found user, updating...')
                conn.execute('''
                UPDATE Users SET historyId = ? WHERE user_email = ?''',
                (new_historyId, user_email)
                )
            else:
                log_print('User not found, adding...')
                conn.execute('''
                INSERT INTO Users (user_email, historyId)
                VALUES (?, ?)''',
                (user_email, new_historyId)
                )
        conn.close()

    def add_new_user(
        user_email,
        database=DATABASE):

        conn = sqlite3.connect(database)
        with conn:
            fetched_user = Users.query_database(user_email, database)
            if not fetched_user:
                conn.execute('''
                INSERT INTO Users (user_email, historyId)
                VALUES (?, ?)''',
                (user_email, None)
                )
        conn.close()

    def grab_all_users(
        database=DATABASE):

        conn = sqlite3.connect(database)
        list_of_users = list()
        with conn:
            for row in conn.execute('SELECT user_email FROM USERS'):
                list_of_users.append(row[0])
        conn.close()

        return list_of_users

class Projects():

    def query_database(
        project_number,
        database=DATABASE):

        conn = sqlite3.connect(database)
        with conn:
            row = conn.execute('''
            SELECT driveId, mail_folderId FROM Projects WHERE project_number = ?''',
            (project_number, )).fetchone()
        conn.close()

        return row

    def add_new_project(project_number, database=DATABASE):
        fetched_project = Projects.query_database(project_number, database)
        if not fetched_project:
            conn = sqlite3.connect(database)
            with conn:
                conn.execute('''
                INSERT INTO Projects (project_number, driveId, mail_folderId)
                VALUES (?, ?, ?)''',
                (project_number, None, None)
                )
            conn.close()

    def update_driveId(
        project_number,
        new_driveId,
        database=DATABASE):

        conn = sqlite3.connect(database)
        with conn:
            #first check if the project is registered
            fetched_project = Projects.query_database(project_number, database)
            if fetched_project:
                log_print('Found project, updating...')
                conn.execute('''
                UPDATE Projects SET driveId = ? WHERE project_number = ?''',
                (new_driveId, project_number)
                )
            else:
                log_print('Project not found, adding...')
                conn.execute('''
                INSERT INTO Projects (project_number, driveId)
                VALUES (?, ?)''',
                (project_number, new_driveId)
                )
        conn.close()

    def update_mail_folderId(
        project_number,
        new_mail_folderId,
        database=DATABASE):

        conn = sqlite3.connect(database)
        with conn:
            #first check if the project is registered
            fetched_project = Projects.query_database(project_number, database)
            if fetched_project:
                log_print('Found project, updating...')
                conn.execute('''
                    UPDATE Projects SET mail_folderId = ? WHERE project_number = ?''',
                    (new_mail_folderId, project_number)
                )
            else:
                log_print('ERROR: Project not found...')
        conn.close()

class Threads():

    def query_database(
        project_number,
        database=DATABASE):

        results = list()
        conn = sqlite3.connect(database)
        with conn:
            sql_query = 'SELECT threadId FROM Threads WHERE project_number = ?'
            for row in conn.execute(sql_query, (project_number, )):
                results.append(row[0])
        conn.close()

        return results

    def query_projects(
        threadId,
        database=DATABASE):

        log_print(f'Looking for project for threadId : {threadId}')
        conn = sqlite3.connect(database)
        with conn:
            row = conn.execute('''
                SELECT project_number FROM Threads WHERE threadId = ?''',
                (threadId, )
            ).fetchone()
            if row:
                row = row[0]
                log_print(f'The thread {threadId} belongs in {row}')
            else:
                log_print(f'The thread {threadId} is not in a Project')
        conn.close()

        return row

    def add_new_thread(
        project_number,
        threadId,
        credentials,
        database=DATABASE):

        existing_threads = Threads.query_database(project_number)
        log_print(f'Existing threads : {existing_threads}')
        if threadId not in existing_threads:
            log_print(f'Adding thread {threadId} to database...')
            conn = sqlite3.connect(database)
            with conn:
                conn.execute('''
                    INSERT INTO Threads (project_number, threadId)
                    VALUES (?, ?)''',
                    (project_number, threadId)
                )
            log_print(f'Added {threadId} to threads for {project_number}')
            conn.close()

            return True

if __name__ == '__main__':
    print(Threads.query_projects('178e20549769ecad'))
    print(Threads.query_database(1256))
