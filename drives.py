from parameters import *
from google.oauth2 import service_account #service account capability
from googleapiclient.discovery import build #build class
from googleapiclient.errors import HttpError
from googleapiclient.http import MediaFileUpload
from datetime import datetime
import os #
import database
import random
import mail_parser
import shutil

'''
    <--Funes v1.0-->
    Funes is a python daemon meant to help a Google Workspace admin backup the
    company emails.

    Copyright (C) 2021 Simón Jaramillo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''
def log_print(*args):
    '''
    Adds datetime.datetime.today() to every print for better logging
    '''
    print(datetime.today(), *args)

class Buffers():

    def create_buffer(project_number=None):
        if not project_number:
            log_print("Can't create buffer for non-project")
        else:
            try:
                os.makedirs(f'buffers/{project_number}')
                log_print(f'Created buffer: buffers/{project_number}')
            except FileExistsError:
                log_print(f'buffer for this project already exists')
            finally:
                return f'buffers/{project_number}'


    def create_message_buffer(messageId, project_number=None):
        if not project_number:
            log_print("Can't buffer message for non-project")
            return None
        else:
            try:
                os.makedirs(f'buffers/{project_number}/{messageId}')
                log_print(f'created message dir buffers/{project_number}/{messageId}')
            except FileExistsError:
                log_print(f'This message already has a buffer')
            finally:
                return f'buffers/{project_number}/{messageId}'

    def buffer_cleanup(project_number, messageId):
        log_print('Starting buffer cleanup...')
        for times in range(5):
            working_dir = os.getcwd()
            buffer_path = f'buffers/{project_number}/{messageId}'
            full_path = os.path.join(working_dir, buffer_path)
            if not os.path.exists(full_path):
                log_print('Path does not exist')
                break
            shutil.rmtree(full_path)
        if os.path.exists(full_path):
            log_print('Unable to clean buffer')
        else:
            log_print('buffer cleanup complete!')

class Drives():

    def create_drive(project_number: str, credentials, rebuild=False):
        driveId = None
        delegated_credentials = credentials.with_subject('administrador@subterranea.cl')
        drive_service = build('drive', 'v3', credentials=delegated_credentials)
        exists = bool(database.Projects.query_database(project_number))
        if rebuild:
            exists = False
        if not exists:
            drive_body = DRIVE_TEMPLATE.copy()
            drive_body['name'] = project_number
            requestId = str(random.randbytes(15))
            drive_query = None
            while True:
                try:
                    drive_query = drive_service.drives().create(
                        requestId=requestId,
                        fields='id',
                        body=drive_body).execute()
                    break
                except HttpError as err:
                    if err.resp.status == '409':
                        requestId = str(random.randbytes(15))
                        continue
                    else:
                        return None

            if drive_query:
                new_driveId = drive_query.get('id', None)
                database.Projects.update_driveId(project_number, new_driveId)
                log_print(f'Drive for {project_number} successfully created')

                log_print('Sharing Drive...')
                permissions = SHARING_TEMPLATE.copy()
                permissions['type'] = 'group'
                permissions['emailAddress'] = 'administradores@subterranea.cl'
                permissions['role'] = 'organizer'
                drive_service.permissions().create(
                    fileId = new_driveId,
                    emailMessage = f'Se ha creado un Drive para el proyecto {project_number}',
                    supportsAllDrives = True,
                    body = permissions
                    ).execute()
                log_print('Drive shared!')
        else:
            log_print(f'Project {project_number} already has a drive!')
            driveId = database.Projects.query_database(project_number)[0]
            log_print(f'Checking Integrity of Drive {driveId}')
            if not driveId:
                return Drives.create_drive(project_number, credentials, rebuild=True)
            try:
                drive_service.drives().get(driveId=driveId).execute()
                log_print('Drive OK')
            except HttpError as err:
                log_print(f'ERROR: {err.resp.status}')
                if err.resp.status == 404:
                    log_print('Drive not found! Rebuilding...')
                    return Drives.create_drive(project_number, credentials, rebuild=True)

        return driveId


    def create_mail_folder(project_number, credentials, rebuild=False):
        delegated_credentials = credentials.with_subject('administrador@subterranea.cl')
        drive_service = build('drive', 'v3', credentials=delegated_credentials)
        row = database.Projects.query_database(project_number)
        driveId = row[0]
        if rebuild:
            row = (driveId, False)
        if not row[1]:
            log_print(f'Building MailFolder for project {project_number}')
            mail_folder_metadata = {
                'name': 'Mails',
                'mimeType': 'application/vnd.google-apps.folder',
                'parents': [driveId]
                }
            mail_folder = drive_service.files().create(
                body=mail_folder_metadata,
                fields='id',
                supportsAllDrives=True).execute()

            mail_folderId = mail_folder.get('id', None)
            log_print(f'Created Mails folder for project {project_number}')
            log_print('Updating Database...')
            database.Projects.update_mail_folderId(project_number, mail_folderId)
        else:
            mail_folderId = row[1]
            log_print('Checking Integrity of MailFolder')
            try:
                drive_service.files().get(
                    fileId=mail_folderId,
                    supportsAllDrives=True
                ).execute()
                log_print('MailFolder OK')
            except HttpError as err:
                log_print(f'ERROR: {err.resp.status}')
                if err.resp.status == 404:
                    log_print('MailFolder not found! Rebuilding...')
                    return Drives.create_mail_folder(project_number, credentials, rebuild=True)
        return mail_folderId


def upload_message(
    project_number,
    message,
    message_headers,
    mail_address,
    credentials):

    log_print('=======[Started Upload Process]======')
    delegated_credentials = credentials.with_subject(ADMIN_ACCOUNT)
    drive_service = build('drive', 'v3', credentials=delegated_credentials)
    user_credentials = credentials.with_subject(mail_address)
    gmail_service = build('gmail', 'v1', credentials=user_credentials)

    messageId = message.get('id')
    driveId, mail_folderId = database.Projects.query_database(project_number)
    log_print(f'DriveId = {driveId}, MailFolderId = {mail_folderId}')
    date = message_headers.get('Date')
    subject = message_headers.get('Subject')

    search = drive_service.files().list(
        corpora='drive',
        driveId=driveId,
        spaces='drive',
        supportsAllDrives=True,
        includeItemsFromAllDrives=True,
        q=f"name='{subject} ; {date}'").execute()
    results = search.get('files', list())
    for result in results:
        if result.get('name', str()) == f'{subject} ; {date}':
            log_print('This message has already been uploaded !!!')
            log_print('=======[Finished Upload Process]======')
            Buffers.buffer_cleanup(project_number, messageId)
            return

    message_folder_metadata = {
        'name': f'{subject} ; {date}',
        'mimeType': 'application/vnd.google-apps.folder',
        'parents': [mail_folderId]
        }
    message_folder = drive_service.files().create(
        body=message_folder_metadata,
        fields='id',
        supportsAllDrives=True).execute()
    message_folderId = message_folder.get('id', None)
    log_print(f'Successfully created folder {message_folderId}')

    attachments = mail_parser.extract_attachments(message)
    log_print(f'Parsing attachments')
    log_print(f'Amount of attachments: {len(attachments)}')
    for attachment in attachments:
        attachmentId = attachment['body']['attachmentId']
        filename = attachment.get('filename', 'untitled')
        log_print(f'Parsing attachment {attachmentId}, ({filename})')
        query = gmail_service.users().messages().attachments().\
        get(
        userId='me',
        messageId=messageId,
        id=attachmentId
        ).execute()
        mail_parser.buffer_attachment(
            attachment,
            messageId,
            project_number,
            gmail_service)
        headers = mail_parser.extract_attachment_headers(attachment)
        mimetype = headers.get('Content-Type', None)
        attachment_metadata = {
            'name': filename,
            'parents': [message_folderId]
            }
        if mimetype:
            attachment_mimetype = mimetype.split(';')[0]
            attachment_metadata['mimeType'] = attachment_mimetype
        media = MediaFileUpload(
            f'buffers/{project_number}/{messageId}/{filename}',
            mimetype=attachment_mimetype,
            resumable=True)
        attachment_upload = drive_service.files().create(
            body=attachment_metadata,
            media_body=media,
            fields='id',
            supportsAllDrives=True).execute()

        file_id = attachment_upload.get('id')

        log_print('Protecting File...')
        permissions = SHARING_TEMPLATE.copy()
        permissions['type'] = 'domain'
        permissions['domain'] = 'subterranea.cl'
        permissions['role'] = 'reader'
        permissions['allowFileDiscovery'] = True
        drive_service.permissions().create(
            fileId = file_id,
            sendNotificationEmail = False,
            supportsAllDrives=True,
            body = permissions
            ).execute()
        log_print('File protected!')
    #upload the message body
    if os.path.exists(f'buffers/{project_number}/{messageId}/body.txt'):
        body_metadata = {
            'name': f'{subject} ; {date}',
            'parents': [message_folderId]
            }
        body_media = MediaFileUpload(
            f'buffers/{project_number}/{messageId}/body.txt',
            resumable=True)
        body = drive_service.files().create(
            body=body_metadata,
            media_body=body_media,
            fields='id',
            supportsAllDrives=True).execute()
        file_id = body.get('id')

        log_print('Protecting File...')
        permissions = SHARING_TEMPLATE.copy()
        permissions['type'] = 'domain'
        permissions['domain'] = 'subterranea.cl'
        permissions['role'] = 'reader'
        permissions['allowFileDiscovery'] = True
        drive_service.permissions().create(
            fileId = file_id,
            sendNotificationEmail = False,
            supportsAllDrives=True,
            body = permissions
            ).execute()
        log_print('File protected!')

    log_print('=======[Finished Upload Process]======')
    Buffers.buffer_cleanup(project_number, messageId)
