from google.oauth2 import service_account #service account capability
from googleapiclient.discovery import build #build class
from parameters import USEFUL_HEADERS, LOGGING
from datetime import datetime
import database
import drives
import os.path
import base64
import json

'''
    <--Funes v1.0-->
    Funes is a python daemon meant to help a Google Workspace admin backup the
    company emails.

    Copyright (C) 2021 Simón Jaramillo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''

def log_print(*args):
    '''
    Adds datetime.datetime.today() to every print for better logging
    '''
    print(datetime.today(), *args)

def extract_headers(message: dict) -> dict:
    message_payload = message.get('payload', dict())
    if message_payload:
        raw_headers = message_payload.get('headers', list())
    else:
        raw_headers = message.get('headers', list())
    extracted_headers = dict()
    for header in raw_headers:
        if header['name'] in USEFUL_HEADERS:
            name = header['name']
            value = header['value']
            extracted_headers[name] = value
    return extracted_headers


def multipart_diver(message, data):
    mimeType = message.get('mimeType', '')
    headers = extract_headers(message)
    if 'multipart' in mimeType:
        log_print("DIVING!")
        parts = message.get('parts', list())
        for part in parts:
            log_print('Checking partId:',  part.get('partId'))
            data = multipart_diver(part, data)

    elif 'text/plain' in mimeType:
        data_buffer = message.get('body', dict()).get('data', str())
        data_buffer = base64.urlsafe_b64decode(data_buffer).decode().strip()
        if len(data_buffer) > 0:
            log_print('FOUND TEXT')
            data += '\n' + data_buffer

    return data

def extract_body(message, project_number):
    messageId = message.get('id')
    message_payload = message.get('payload', dict())
    mimeType = message_payload.get('mimeType', '')
    headers = extract_headers(message)
    data = str()
    buffer = drives.Buffers.create_message_buffer(messageId, project_number)
    log_print(f'building body in {buffer}/body.txt')
    if 'multipart' in mimeType:
        log_print(f'message {messageId} is a multipart message')
        log_print(f'Deploying Multipart Diver')
        data = multipart_diver(message_payload, data)

    elif 'text/plain' in mimeType:
        data = message_payload.get('body', dict()).get('data', str())
        data = base64.urlsafe_b64decode(data).decode()
    with open(f'{buffer}/body.txt', 'w') as file:
        file.write(f'Date:\t\t\t{headers.get("Date", str())}\n')
        file.write(f'To:\t\t\t{headers.get("To", str())}\n')
        file.write(f'From:\t\t\t{headers.get("From", str())}\n')
        file.write(f'Subject:\t\t{headers.get("Subject", str())}\n')
        file.write(f'\n\n{data}')
    log_print(f'Message {messageId} parsed succesfully')


def extract_attachment_headers(attachment: dict):
    raw_headers = attachment.get('headers', list())
    extracted_headers = dict()
    for header in raw_headers:
        # if header['name'] in USEFUL_HEADERS:
        name = header['name']
        value = header['value']
        extracted_headers[name] = value
    return extracted_headers

def multipart_attachment_diver(message, list_of_attachments):
    mimeType = message.get('mimeType', '')
    headers = extract_headers(message)
    if 'multipart' in mimeType:
        log_print("DIVING!")
        parts = message.get('parts', list())
        for part in parts:
            log_print('Checking partId:',  part.get('partId'))
            multipart_attachment_diver(part, list_of_attachments)

    else:
        attachmentId = message['body'].get('attachmentId', None)
        if attachmentId:
            list_of_attachments.append(message)
            log_print('FOUND Attachment')
            log_print('partId:',  message.get('partId'))

    log_print(f'Attachment count:{len(list_of_attachments)}')
    return list_of_attachments

def extract_attachments(message: dict) -> list:
    payload = message.get('payload', dict())
    mimetype = payload.get('mimeType', None)
    list_of_attachments = list()

    if not mimetype:
        log_print('Mimetype not found!')

    if 'multipart' in mimetype:
        log_print(f'Deploying Attachment Multipart Diver')
        list_of_attachments = multipart_attachment_diver(payload, list_of_attachments)

    return list_of_attachments

def buffer_attachment(attachment, messageId, project_number, service):
    attachmentId = attachment.get('body', dict()).get('attachmentId')
    filename = attachment.get('filename', 'untitled')
    attachment_query = service.users().messages().attachments().get(
        userId='me',messageId=messageId,
        id=attachmentId).execute()

    buffer = drives.Buffers.create_buffer(project_number)
    mess_dir = drives.Buffers.create_message_buffer(messageId, project_number)
    file_counter = 1
    while os.path.exists(f'{mess_dir}/{filename}'):#this is incomplete
        filename = filename.split('.')
        filename.insert(-1, f'({file_counter})')
        filename.insert(-1, '.')
        filename = str().join(filename)
    with open(f'{mess_dir}/{filename}', 'wb') as att_file:
        data = attachment_query.get('data', str())
        data = base64.urlsafe_b64decode(data.encode('UTF-8'))
        att_file.write(data)

    #log the attachment for future reference
    if LOGGING:
        with open('attachment_log.json', 'a') as log:
            log.write(json.dumps(attachment, indent=4))
            log.write('\n\n')
            log.write(json.dumps(attachment_query, indent=4))
            log.write('\n\n\n')


def look_for_project(message: dict, credentials):
    messageId = message.get('id', None)
    threadId = message.get('threadId', None)
    headers = extract_headers(message)
    subject = headers.get('Subject', '')
    project_number = None
    mess_dir = None
    new_thread = False
    if '[' in subject:
        start = subject.find('[') + 1
        end = subject.find(']')
        if end != -1:
            project = subject[start:end]

            if project.isnumeric():
                project_number = project
                new_thread = bool(database.Threads.add_new_thread(
                    project_number,
                    threadId,
                    credentials
                ))
            else:
                log_print('This message is not in a project')

    else:
        project_number = database.Threads.query_projects(threadId)
        if project_number:
            log_print(f'THIS MESSAGE BELONGS TO PROJECT {project_number}')
        else:
            log_print('This message is not in a project')

    return project_number, new_thread
