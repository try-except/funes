from google.oauth2 import service_account #service account capability
from googleapiclient.discovery import build #build class
from googleapiclient.errors import HttpError
from parameters import SCOPES, SERVICE_ACCOUNT_FILE, LOGGING, ADMIN_ACCOUNT
from datetime import datetime
import database
import drives
import mail_parser
import os
import json

'''
    <--Funes v1.0-->
    Funes is a python daemon meant to help a Google Workspace admin backup the
    company emails.

    Copyright (C) 2021 Simón Jaramillo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''

def log_print(*args):
    '''
    Adds datetime.datetime.today() to every print for better logging
    '''
    print(datetime.today(), *args)

def initial_program_setup():
    '''
    Creates the database and fills it with the users in the specified domain
    '''
    #Create the service account credentials object
    credentials = service_account.Credentials.from_service_account_file(
        SERVICE_ACCOUNT_FILE, scopes=SCOPES)

    #Create the database and fill it with the users in the domain
    database.create_database()
    database.get_users(credentials)
    counter_for_user_check = 15 #Set a counter for next new users check
    return credentials, counter_for_user_check

def list_search(
    message_source: list,
    credentials,
    gmail_service,
    mail_address,
    retrieved_messages: list,
    unretrieved: list):
    '''
    Retrieves messages from a messageId list.
    '''
    log_print('¡NEW MESSAGES!')
    message = None
    while message_source:
        #Check each message
        if not message:
            message = message_source.pop()
            messageId = message.get('id')
            threadId = message.get('threadId')
            #Prevent messages being retrieved multiple times
            if messageId in retrieved_messages:
                log_print('This message has already been retrieved')
                message = None
                continue

        log_print(f'RETRIEVING MESSAGE ID: {messageId}')
        log_print(f'This message is in thread: {threadId}')
        try:
            message_query = gmail_service.users().messages().get(
                userId='me',
                id=messageId
                ).execute()

            if LOGGING:
                #log the response for future reference
                with open('message_query_log.json', 'a') as log:
                    log.write(f'=========[{datetime.today()}]=========\n')
                    log.write(json.dumps(message_query, indent=4))
                    log.write('\n\n\n')

            #Check if it's a draft, if it is skip it
            labels = message_query.get("labelIds", list())
            if 'DRAFT' in labels:
                log_print('This message is a draft')
                message = None
                continue

            #Check for a project number in the subject
            project_number, new_thread = mail_parser.look_for_project(
                message_query,
                credentials
            )

            #Check for a project_number related to the message thread
            if not project_number:
                project_number = database.Threads.query_projects(threadId)
                if not project_number:
                    message = None
                    continue

            if new_thread:
                thread_query = gmail_service.users().threads().get(
                    userId='me',
                    id=threadId
                ).execute()
                thread_messages = thread_query.get('messages')
                log_print(f'Checking thread: {threadId}')
                log_print(f'Amount of messages in thread: {len(thread_messages)}')
                list_search(
                    thread_messages,
                    credentials,
                    gmail_service,
                    mail_address,
                    retrieved_messages,
                    unretrieved
                )

            #Register the project number and retrieve driveId
            database.Projects.add_new_project(project_number)
            drives.Buffers.create_buffer(project_number)
            drives.Buffers.create_message_buffer(messageId, project_number)
            driveId = drives.Drives.create_drive(project_number, credentials)
            mail_folderId = drives.Drives.create_mail_folder(project_number, credentials)
            #Parse the retrieved message
            headers = mail_parser.extract_headers(message_query)

            if LOGGING:
                #log the headers for future reference
                with open('headers_log.json', 'a') as log:
                    log.write(json.dumps(headers, indent=4))
                    log.write('\n\n\n')

            mail_parser.extract_body(message_query, project_number)
            message_payload = message_query.get('payload', '')

            if LOGGING:
                #log the payload for future reference
                with open('message_payload_log.json', 'a') as log:
                    log.write(json.dumps(message_payload, indent=4))
                    log.write('\n\n\n')

            log_print(f'Succesfully retrieved message {messageId}\n')
            retrieved_messages.append(messageId)
            #Begin uploading the message
            drives.upload_message(
                project_number,
                message_query,
                headers,
                mail_address,
                credentials)
            log_print(f'Succesfully uploaded message {messageId}')
            #Continue with next message
            message = None

        except HttpError as err:
            log_print(f'ERROR: {err.resp.status}')
            if err.resp.get('content-type', '').startswith('application/json'):
                err_message = json.loads(err.content).get('error').get('errors')[0].get('message')
                reason = json.loads(err.content).get('error').get('errors')[0].get('reason')
                print(reason)
                print(err_message)

            # If the error is a rate limit or connection error,
            # wait and try again.
            if err.resp.status in [403, 500, 503]:
                log_print(f'retrying...')
                time.sleep(5)

            #if the message was not found, add it to unretrieved
            elif err.resp.status == 404:
                print(message)
                unretrieved.append(message)
                message = None

            #if else, print an error message and continue
            else:
                log_print(f'Failed to retrieve message: {messageId}')
                message = None

        except Exception as err:
            print(type(err).__name__)
            print(err)
            print(err.traceback.format_exc())

        finally:
            continue
    log_print(f'THIS USERS UNRETRIEVED MESSAGES:')
    print(unretrieved)

def do_main_program(credentials, counter_for_user_check):
    '''
    This is the main program flow. It is intended to run as a Daemon.
    '''
    if counter_for_user_check <= 0:
        credentials, counter_for_user_check = initial_program_setup()
    #Check each user's mailbox for changes
    users = database.Users.grab_all_users()
    for mail_address in users:
        log_print('USING CREDENTIALS:', mail_address)
        delegated_credentials = credentials.with_subject(mail_address)
        gmail_service = build('gmail', 'v1', credentials=delegated_credentials)
        previous_historyId = database.Users.query_database(mail_address)[0]
        log_print(f'Current historyId: {previous_historyId}')

        if not previous_historyId: #if this is the first time
            api_query = gmail_service.users().getProfile(userId='me').execute()
            historyId = api_query.get('historyId', None)

            if historyId:
                database.Users.update_historyId(mail_address, historyId)
                log_print(f'Added historyId {historyId} for user {mail_address}')
            else:
                log_print(f'Failed to retrieve historyId for {mail_address}')

            continue

        history_query = gmail_service.users().history().list(
            userId='me',
            startHistoryId=previous_historyId
            ).execute()
        new_historyId = history_query.get('historyId', None)

        if new_historyId == previous_historyId:
            log_print( 'No new history')
            continue

        log_print(f'Checking historyId: {new_historyId}')
        grabbed_history = history_query.get('history', None)

        if not grabbed_history:
            log_print( 'No new messages')
            continue

        if LOGGING:
            with open('history_dump.json', 'a') as file:
                file.write(f'=========[{mail_address}]========\n')
                file.write(f'=========[{datetime.today()}]=========\n')
                file.write(json.dumps(grabbed_history[0], indent=4))

        retrieved_messages = list()
        unretrieved = list()
        for history in grabbed_history:
            messages = history.get('messages', list())
            list_search(
                messages,
                credentials,
                gmail_service,
                mail_address,
                retrieved_messages,
                unretrieved
            )
            database.Users.update_historyId(mail_address, new_historyId)

    counter_for_user_check -= 1

if __name__ == '__main__':
    credentials, counter_for_user_check = initial_program_setup()
    while True:
        do_main_program(credentials, counter_for_user_check)
