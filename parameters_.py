SCOPES = [
    'https://www.googleapis.com/auth/gmail.readonly',
    'https://www.googleapis.com/auth/admin.directory.user.readonly',
    'https://www.googleapis.com/auth/drive',
    ]

SERVICE_ACCOUNT_FILE = 'service_credentials.json'
DATABASE = 'funes.sqlite'

DOMAIN = ''
ADMIN_ACCOUNT = ''

LOGGING = False
DRYRUN = False

USEFUL_HEADERS = [
    'To',
    'From',
    'Subject',
    'Date',
    'MIME-Version',
    'Content-Type',
    ]

DRIVE_TEMPLATE = {
  "kind": "drive#drive",
  "id": str(),
  "name": str(),
  "capabilities": {
    "canAddChildren": True,
    "canChangeCopyRequiresWriterPermissionRestriction": False,
    "canChangeDomainUsersOnlyRestriction": False,
    "canChangeDriveBackground": False,
    "canChangeDriveMembersOnlyRestriction": True,
    "canComment": True,
    "canCopy": True,
    "canDeleteChildren": False,
    "canDeleteDrive": False,
    "canDownload": False,
    "canEdit": True,
    "canListChildren": True,
    "canManageMembers": True,
    "canReadRevisions": False,
    "canRename": True,
    "canRenameDrive": True,
    "canShare": True,
    "canTrashChildren": False
  },
  "hidden": False,
  "restrictions": {
    "adminManagedRestrictions": True,
    "copyRequiresWriterPermission": False,
    "domainUsersOnly": False,
    "driveMembersOnly": False
  }
}

SHARING_TEMPLATE = {
  "kind": "drive#permission",
  "type": str(),
  "role": str()
}
